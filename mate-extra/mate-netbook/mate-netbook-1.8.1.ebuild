# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils gnome2-utils

DESCRIPTION="A simple window management tool"
HOMEPAGE="http://mate-desktop.org"
SRC_URI="http://pub.mate-desktop.org/releases/1.8/${PN}-${PV}.tar.xz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="
	>=mate-base/mate-panel-1.8.0"
DEPEND="
	app-text/yelp-tools
	dev-perl/XML-Parser
	x11-libs/libXtst
	x11-libs/gtk+:2
	x11-libs/libfakekey
	x11-libs/libwnck:1
	dev-libs/libunique:1
	>=mate-base/mate-common-1.8.0"

S="${WORKDIR}/${PN}-${PV}"

src_configure() {
	econf \
		--with-gtk=2.0
		--disable-static
}

src_compile() {
	emake || die "compilation failed"
}

src_install() {
	default
}

pkg_postinst() {
	gnome2_schemas_update
}

pkg_postrm() {
	gnome2_schemas_update
}
