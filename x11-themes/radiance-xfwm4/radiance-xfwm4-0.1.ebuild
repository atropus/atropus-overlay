# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

DESCRIPTION="Radiance theme for Xfwm"
HOMEPAGE="http://xfce-look.org/content/show.php/Radiance-xfwm4?content=161714"
SRC_URI="http://xfce-look.org/CONTENT/content-files/161714-${PN}-${PV}.tar.gz"

LICENSE="GPL"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

DEPEND="xfce-base/xfwm4"
RDEPEND="${DEPEND}"

S="${WORKDIR}"


src_install() {
	cd "${S}"
	dodir /usr/share/themes/${PN} || die
	insinto /usr/share/themes/${PN} || die
	doins -r "${S}"/${PN}-${PV}/${PN}/* || die
}

