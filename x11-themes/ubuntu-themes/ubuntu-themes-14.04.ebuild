# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5
inherit eutils

MY_PV="14.04.20140410"

DESCRIPTION="GTK2/GTK3 Ambiance and Radiance themes from Ubuntu"
HOMEPAGE="https://launchpad.net/light-themes"
SRC_URI="http://archive.ubuntu.com/ubuntu/pool/main/u/${PN}/${PN}_${PV}+${MY_PV}.orig.tar.gz"

LICENSE="CC-BY-SA-3.0 CC-BY-SA-4.0"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="gtk3"

DEPEND="!x11-themes/light-themes"
RDEPEND="
	x11-themes/gtk-engines-murrine
	gtk3? ( x11-themes/gtk-engines-unico )"

S="${WORKDIR}/${PN}-${PV}+${MY_PV}"

src_install() {
	insinto /usr/share/themes
	doins -r Radiance* Ambiance*

	use gtk3 || {
		rm -R "${D}"/usr/share/themes/*/gtk-3.0 || die
	}
}
