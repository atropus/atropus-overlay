# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit eutils git-2

EGIT_BRANCH="4.1-atropus"
EGIT_COMMIT="7035ed2c8c6d762bdeaea14c3ae1cc2524a622af"
EGIT_REPO_URI="https://gitlab.com/atropus/atropus-kernel-sources.git"

DESCRIPTION="Atropus Linux Kernel Sources"
LICENSE="GPL-2"
EXTRAVERSION=atropus-r1
KEYWORDS="amd64 x86"
RESTRICT="binchecks strip mirror"

IUSE=""
DEPEND="
	sys-devel/bc
	dev-vcs/git"
RDEPEND="${DEPEND}"

SLOT=$PVR
CKV=4.1.5
KV_FULL=${PN}-${PVR}



S="$WORKDIR/linux-${CKV}"

pkg_setup() {
	export REAL_ARCH="$ARCH"
	unset ARCH ; unset LDFLAGS #will interfere with Makefile if set
}

src_compile() {
	make -s mrproper || die "make mrproper failed"
}

src_install() {
	dodir /usr/src/linux-${PV}-${EXTRAVERSION} || die
	insinto /usr/src/linux-${PV}-${EXTRAVERSION} || die
	doins -r "${S}"/* || die
	cd ${D}/usr/src/linux-${PV}-${EXTRAVERSION} || die
	make mrproper || die
}

