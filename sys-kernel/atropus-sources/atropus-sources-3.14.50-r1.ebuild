# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit eutils git-2

EGIT_BRANCH="3.14-atropus"
EGIT_COMMIT="2a0ea2ef92c55ff2581c6bf8d2a9201db2d7d2cd"
EGIT_REPO_URI="https://gitlab.com/atropus/atropus-kernel-sources.git"

DESCRIPTION="Atropus Linux Kernel Sources"
LICENSE="GPL-2"
EXTRAVERSION=atropus-r1
KEYWORDS="amd64 x86"
RESTRICT="binchecks strip mirror"

IUSE=""
DEPEND="
	sys-devel/bc
	dev-vcs/git"
RDEPEND="${DEPEND}"

SLOT=$PVR
CKV=3.14.50
KV_FULL=${PN}-${PVR}



S="$WORKDIR/linux-${CKV}"

pkg_setup() {
	export REAL_ARCH="$ARCH"
	unset ARCH ; unset LDFLAGS #will interfere with Makefile if set
}

src_compile() {
	make -s mrproper || die "make mrproper failed"
}

src_install() {
	dodir /usr/src/linux-${PV}-${EXTRAVERSION} || die
	insinto /usr/src/linux-${PV}-${EXTRAVERSION} || die
	doins -r "${S}"/* || die
	cd ${D}/usr/src/linux-${PV}-${EXTRAVERSION} || die
	make mrproper || die
}

