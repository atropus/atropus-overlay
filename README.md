atropus-overlay
========

Atropus Gentoo Overlay

All ebuilds from this overlay are marked as unstable, use package.keywords to enable installation.

To install it as an overlay in Gentoo && Gentoo based distros, use the following command :

layman -f -a atropus-overlay -o https://gitlab.com/atropus/atropus-overlay/raw/master/overlay.xml
