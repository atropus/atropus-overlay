# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2


EAPI=5

inherit eutils

DESCRIPTION="Keeps track of EVERY kernel module ever used - useful for those of us who make localmodconfig :)"
HOMEPAGE="https://wiki.archlinux.org/index.php/Modprobed-db"
SRC_URI="http://repo-ck.com/source/${PN}/${PN}-${PV}.tar.xz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="sys-apps/kmod"
RDEPEND="sys-apps/kmod"

S=${WORKDIR}/${PN}-${PV}

src_compile() {
	cd "${S}"
	make || die "Make failed"
}

src_install() {
	default
}
