# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2


EAPI=5

inherit eutils

DESCRIPTION="Injects the mvps blocklist to /etc/hosts to prevent thousands of parasites, hijackers and unwanted adware/spyware/privacy websites from working."
HOMEPAGE="https://github.com/graysky2/hosts-update"
SRC_URI="http://repo-ck.com/source/${PN}/${PN}-${PV}.tar.xz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="net-misc/curl"
RDEPEND="net-misc/curl"

S=${WORKDIR}/${PN}-${PV}

src_compile() {
	cd "${S}"
	make || die "Make failed"
}

src_install() {
	default
}
