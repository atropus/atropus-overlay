# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit eutils distutils git-2

EGIT_BRANCH=master
EGIT_REPO_URI="https://github.com/mgedmin/irclog2html"

DESCRIPTION="IRC log file colouriser"
HOMEPAGE="https://mg.pov.lt/irclog2html/"
SRC_URI=""
S="${WORKDIR}/${PN}"

LICENSE="GPL"
SLOT="0"
KEYWORDS="amd64 x86"

RDEPEND="dev-lang/python"
DEPEND="${RDEPEND}"

src_install() {
	distutils_src_install || die "install failed"
}

