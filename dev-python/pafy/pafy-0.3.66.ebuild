# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit eutils distutils

DESCRIPTION="Python API for Youtube"
HOMEPAGE="http://np1.github.io/pafy"
SRC_URI="https://github.com/np1/${PN}/archive/v${PV}.tar.gz"
S="${WORKDIR}/${PN}-${PV}"

LICENSE="GPL"
SLOT="0"
KEYWORDS="amd64 x86"

RDEPEND="dev-lang/python"
DEPEND="${RDEPEND}"

src_install() {
	distutils_src_install || die "install failed"
}

